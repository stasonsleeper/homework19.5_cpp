// Homework_19.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

class Animal
{
public:
    virtual char Voice()
    {
        cout << "no sound" << endl;
        return 0;
    }
    virtual ~Animal() 
    {
        cout << "Deleting parent class 'Animal'" << endl;
    };
};

class Cat : public Animal
{
    char Voice()
    {
        cout << "Meow!" << endl;
        return 0;
    }

    ~Cat() 
    {
        cout << "Deleting child class 'Cat'" << endl;
    };
};

class Lion : public Animal
{
    char Voice()
    {
        cout << "Roar!" << endl;
        return 0;
    }
    
    ~Lion()
    {
        cout << "Deleting child class 'Lion'" << endl;
    };
};

class Dog : public Animal
{
    char Voice()
    {
        cout << "Woof!" << endl;
        return 0;
    }

    ~Dog()
    {
        cout << "Deleting child class 'Dog'" << endl;
    };
};


int main()
{
    Animal* c = new Cat;
    Animal* l = new Lion;
    Animal* d = new Dog;
    
    
    Animal *Arr[3]{ c, l, d };
    for (int i = 0; i < 3; i++)
    {
        cout << Arr[i]->Voice() << endl;
        delete Arr[i];
    }
    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
